[![pipeline status](https://gitlab.com/ModioAB/journal-gelf-proxy/badges/master/pipeline.svg)](https://gitlab.com/ModioAB/journal-gelf-proxy/commits/master)
[![coverage report](https://gitlab.com/ModioAB/journal-gelf-proxy/badges/master/coverage.svg)](https://gitlab.com/ModioAB/journal-gelf-proxy/commits/master)

# Systemd Journal Upload to GELF proxy

Listens to `http://:19532/upload`, to receive exported journal events from `systemd-journal-upload`.
Each event is converted to GELF, and sent to a Graylog server over TCP.

```mermaid
sequenceDiagram

Journal Upload ->> Journal GELF Proxy: HTTP POST /upload
activate Journal Upload
activate Journal GELF Proxy

loop Events
    Journal Upload ->> Journal GELF Proxy: Event
    alt First Event
        Journal GELF Proxy ->> Graylog TCP: Connect
        activate Graylog TCP
    end
    Journal GELF Proxy ->> Graylog TCP: Message
end

Journal GELF Proxy ->> Graylog TCP: Disconnect
deactivate Graylog TCP

Journal GELF Proxy -->> Journal Upload: 200 OK
deactivate Journal GELF Proxy
deactivate Journal Upload
```

## Behind TLS Reverse proxy

In most real world deployments, `journal-gelf-proxy` should be fronted by a TLS reverse proxy, for encryption and log client authentication.

```mermaid
sequenceDiagram

Journal Upload ->> TLS Proxy: Authenticate
activate Journal Upload
activate TLS Proxy
Journal Upload ->> TLS Proxy: HTTP POST /upload
TLS Proxy ->> Journal GELF Proxy: HTTP POST /upload
activate Journal GELF Proxy

loop Events
    Journal Upload ->> Journal GELF Proxy: Event
    alt First Event
        Journal GELF Proxy ->> Graylog TCP: Connect
        activate Graylog TCP
    end
    Journal GELF Proxy ->> Graylog TCP: Message
end

Journal GELF Proxy ->> Graylog TCP: Disconnect
deactivate Graylog TCP

Journal GELF Proxy -->> TLS Proxy: 200 OK
deactivate Journal GELF Proxy
TLS Proxy -->> Journal Upload: 200 OK
deactivate TLS Proxy
deactivate Journal Upload
```
