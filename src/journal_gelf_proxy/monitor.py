"""
This implements HTTP endpoints for monitoring service health
"""

from http import HTTPStatus
from time import monotonic
from twisted.internet.defer import Deferred
from twisted.web.resource import Resource
from twisted.web.server import NOT_DONE_YET

from journal_gelf_proxy.gelf import GELF


class LivenessResponse(Deferred):
    """
    Deferred response to liveness check, to measure RTT
    """

    def __init__(self, request):
        super().__init__()
        self._request = request
        self._start_time = monotonic()

    @property
    def status(self):
        """
        Returns the liveness status as a GELF message
        """
        http_host = self._request.getHeader(b"Host").decode()
        host = http_host.split(":")[0]

        message = GELF()
        message.update(
            host=host,
            short_message="listening for connections",
            _alive=True,
            _ready=None,
            _input_delay=monotonic() - self._start_time,
        )
        return message

    def finish(self):
        """
        Respond with liveness status and finish the request
        """
        response_body = self.status.encode()

        self._request.setHeader(b"Content-Type", b"application/json")
        self._request.setResponseCode(HTTPStatus.OK)
        self._request.write(response_body)
        self._request.finish()


class LivenessCheck(Resource):
    """
    Twisted Web Resource that responds to a liveness check
    """

    def __init__(self, reactor):
        super().__init__()
        self.reactor = reactor

    def render_GET(self, request):  # Not our API, pylint: disable=C0103
        """
        Process a HTTP GET for a liveness check
        """
        accept = request.getHeader(b"Accept")

        if accept == b"application/json":
            response = LivenessResponse(request)
            self.reactor.callLater(0, response.finish)
            return NOT_DONE_YET

        request.setResponseCode(HTTPStatus.NO_CONTENT)
        return b""


class ServiceMonitors(Resource):
    """
    Twisted Web Resource for service monitoring endpoints
    """

    def __init__(self, reactor):
        Resource.__init__(self)
        self.putChild(b"livez", LivenessCheck(reactor))
