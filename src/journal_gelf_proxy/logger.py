"""
A simple stdout/stderr logger for request life cycle information
"""

from json import dumps
from sys import stderr, stdout

REQUEST_HEADERS = {
    b"X-Request-ID": "request_id",
    b"X-Forwarded-For": "remote_addr",
    b"X-CLIENT-SSL-CN": "cid",
}


def get_request_info(request):
    """
    Collect information about a request into a dictionary
    """
    info = {
        "request_id": id(request),
        "client_addr": request.getClientAddress().host,
        "path": request.path.decode(),
        "host": request.getRequestHostname().decode(),
    }
    for header, log_key in REQUEST_HEADERS.items():
        value = request.getHeader(header)
        if value:
            info[log_key] = value.decode()
    return info


class Logger:
    """
    A reusable json log message emitter that memorizes information about a
    request
    """

    def __init__(self, request=None):
        if request:
            self.request_info = get_request_info(request)
        else:
            self.request_info = {}

    def error(self, **message):
        """
        Log errors to stderr
        """
        print(dumps({**message, **self.request_info}), file=stderr, flush=True)

    def info(self, **message):
        """
        Log informational to stdout
        """
        print(dumps({**message, **self.request_info}), file=stdout, flush=True)
