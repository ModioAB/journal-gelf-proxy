"""Entrypoint helpers for container."""

import argparse
from dataclasses import dataclass

from twisted.internet import task

from .listener import journal_upload_listener
from .logger import Logger
from .gelf import GraylogAgent


@dataclass
class Config:
    """Runtime configuration."""

    selftest: bool


def main(reactor, listener_description="tcp:19532"):
    """
    Create a Twisted Internet Endpoint that listens for journal uploads and
    proxies to GELF
    """
    log = Logger()

    agent = GraylogAgent(reactor)
    log.info(event="graylog agent setup", **agent.options)

    log.info(event="starting listener", description=listener_description)
    return journal_upload_listener(reactor, listener_description, graylog_agent=agent)


def test_main(reactor):
    """Run main in self-test mode."""
    listener = main(reactor)
    reactor.callLater(0, reactor.stop)
    return listener


def parse_cmdline() -> Config:
    """Parse the command line and return a config."""
    parser = argparse.ArgumentParser(description="The journal GELF proxy.")
    parser.add_argument(
        "--test-run",
        dest="selftest",
        action="store_true",
        help="Run a self-test and exit instantly",
    )
    args = parser.parse_args()
    return Config(selftest=args.selftest)


def entrypoint():
    """Function entrypoint, muxing test mode and not."""
    config = parse_cmdline()
    if config.selftest:
        task.react(test_main)
    else:
        task.react(main)
