#!/usr/bin/python3
"""
This implements the HTTP endpoint that systemd-journal-upload connects to.
"""
from http import HTTPStatus
from twisted.internet import defer
from twisted.internet.endpoints import serverFromString
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from journal_gelf_proxy.logger import Logger
from journal_gelf_proxy.gelf import GELF
from journal_gelf_proxy.monitor import ServiceMonitors


class Context:  # pylint: disable=R0903
    """
    Request processing context
    """

    def __init__(self):
        self.log = None
        self.request = None
        self.messages = None
        self.message_count = 0
        self.drop_age_count = 0

    def next_message(self):
        """
        Get the next message , if there are any left
        """
        if not self.request:
            return None

        try:
            return next(self.messages)

        except StopIteration:
            self.finish_request(
                respone_code=HTTPStatus.OK,
                event="client finished",
                count=self.message_count,
                too_old=self.drop_age_count,
            )
            return None
        except ValueError:
            self.log.error(
                event="unexpected client disconnection",
                count=self.message_count,
                too_old=self.drop_age_count,
            )
            return None

    def finish_request(self, response_code=HTTPStatus.OK, **log_event):
        """
        Finish the HTTP request, logging the event
        """
        self.request.setResponseCode(response_code)
        self.request.finish()

        if response_code != HTTPStatus.OK:
            self.log.error(**log_event)
        else:
            self.log.info(**log_event)


class JournalEvents:
    """
    Iterator that parses a stream of Systemd Journal events in export format
    into dicts
    """

    def __init__(self, stream):
        self.stream = stream

    def __iter__(self):
        return self

    def __next__(self):
        event = {}

        for line in self.stream:
            if line == b"\n":
                return event

            key, sep, value = line.strip().partition(b"=")

            if sep != b"=":
                # Binary encoded length, not including final \n
                binary_length = self.stream.read(8)
                length = int.from_bytes(binary_length, byteorder="little")
                value = self.stream.read(length)
                self.stream.read(1)

            event[key.decode()] = value

        raise StopIteration


class JournalUpload(Resource):
    """
    Twisted Web Resource that handles journal uploads
    """

    def __init__(self, graylog_agent=None):
        Resource.__init__(self)
        self.graylog_agent = graylog_agent

    def render_POST(self, request):  # Not our API, pylint: disable=C0103
        """
        Process a HTTP POST with systemd journal events in export format
        """
        context = Context()
        context.request = request
        context.log = Logger(request)

        context.log.info(event="client connected")

        content_type = request.getHeader(b"Content-Type")
        if content_type != b"application/vnd.fdo.journal":
            context.log.error(event="invalid content type")
            request.setResponseCode(HTTPStatus.UNSUPPORTED_MEDIA_TYPE)
            return b"Unsupported media type"

        journal_events = JournalEvents(request.content)
        context.messages = (GELF(event) for event in journal_events)

        if self.graylog_agent:
            self.graylog_agent.send(context)
            return NOT_DONE_YET

        request.setResponseCode(HTTPStatus.OK)
        return b"OK"


def journal_upload_listener(reactor, description, graylog_agent=None):
    """
    Create a Twisted Internet Endpoint that listens for journal uploads
    """
    endpoint = serverFromString(reactor, description)

    root = Resource()
    root.putChild(b"upload", JournalUpload(graylog_agent))
    root.putChild(b"-", ServiceMonitors(reactor))
    endpoint.listen(Site(root))

    return defer.Deferred()
