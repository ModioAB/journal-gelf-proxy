#!/usr/bin/python3
"""
This implements conversion of Journal events to GELF messages
"""

import time
from http import HTTPStatus
from json import dumps as json_dumps, loads as json_loads
from os import getenv
from textwrap import wrap
from twisted.internet.protocol import Protocol, ClientFactory

FACILITIES = {
    0: "kern",
    1: "user",
    2: "mail",
    3: "daemon",
    4: "auth",
    5: "syslog",
    6: "lpr",
    7: "news",
    8: "uucp",
    9: "cron",
    10: "authpriv",
    11: "ftp",
    12: "ntp",
    13: "security",
    14: "console",
    16: "local0",
    17: "local1",
    18: "local2",
    19: "local3",
    20: "local4",
    21: "local5",
    22: "local6",
    23: "local7",
}


def map_facility(value):
    """
    Map syslog facility number to facility name
    """
    if value.isdecimal():
        return FACILITIES.get(int(value), f"unknown({value})")
    return value


VALUE_MAP = {
    "PRIORITY": int,
    "SYSLOG_FACILITY": map_facility,
    "__CURSOR": None,
    "__REALTIME_TIMESTAMP": lambda rts: float(rts) / 1e6,
}


def map_value(key, value):
    """
    Map values from journal to GELF, if necessary
    """
    try:
        mapper = VALUE_MAP[key]
    except KeyError:
        return value

    if mapper:
        return mapper(value)
    return None


KEY_MAP = {
    "MESSAGE": "short_message",
    "PRIORITY": "level",
    "SYSLOG_FACILITY": "_facility",
    "_HOSTNAME": "host",
    "__REALTIME_TIMESTAMP": "timestamp",
}


def map_key(key):
    """
    Map keys from journal to GELF
    """
    return KEY_MAP.get(key, "_" + key)


class GELF:
    """
    Representation of av GELF message
    """

    def __init__(self, event=None, json=None):
        self.message = {
            # GELF requires these to exist in a message
            "version": "1.1",
            "host": "localhost",
            "short_message": "",
        }
        if json:
            self.message.update(json_loads(json))
        elif event:
            for key, value in event.items():
                self.journal(key, value)

    def __str__(self):
        """
        The message in GELF json format
        """
        return json_dumps(self.message)

    def __getitem__(self, key):
        """
        Get the value for a key in the message
        """
        return self.message[key]

    def journal(self, key, value):
        """
        Insert key/value from systemd journal, mapping it to GELF
        """
        try:
            value = map_value(key, value.decode())
        except UnicodeDecodeError:
            value = "\n".join(wrap(value.hex(), 64))

        key = map_key(key)
        if value:
            self.message[key] = value

    def update(self, **kwargs):
        """
        Update GELF key/values in the message
        """
        self.message.update(**kwargs)

    def encode(self, *args):
        """
        Encode the message for transmission
        """
        return str(self).encode(*args)


class MessageAgeError(RuntimeError):
    """
    A GELF message is too old to process, and should be dropped
    """


class GraylogProtocolTCP(Protocol):
    """
    Twisted protocol for transmitting GELF messages over TCP
    """

    def __init__(self, agent, context):
        super().__init__()
        self.agent = agent
        self.context = context
        self.http_response_code = HTTPStatus.INTERNAL_SERVER_ERROR

    def connectionMade(self):
        self.send_next_message()

    def send_next_message(self):
        """
        Send the next message to the graylog server
        """
        message = self.context.next_message()

        if message:
            if self.agent.valid_message_age(message):
                self.context.message_count += 1
                self.transport.write(message.encode() + b"\0")
                self.agent.call_with_backoff(self.send_next_message, self.context)

            else:
                self.context.drop_age_count += 1
                self.agent.call_without_backoff(self.send_next_message)

        else:
            self.transport.loseConnection()


class GraylogClientFactory(ClientFactory):
    """
    Twisted factory for creating GELF connection clients
    """

    def __init__(self, agent, context):
        super().__init__()
        self.context = context
        self.agent = agent

    def buildProtocol(self, addr):
        return GraylogProtocolTCP(self.agent, self.context)

    def clientConnectionFailed(self, connector, reason):
        ClientFactory.clientConnectionFailed(self, connector, reason)

        if self.agent.errback:
            self.agent.errback(self, connector, reason)

        self.context.finish_request(
            response_code=HTTPStatus.BAD_GATEWAY, event="upstream connection failure"
        )

    def clientConnectionLost(self, connector, reason):
        ClientFactory.clientConnectionLost(self, connector, reason)

        if self.agent.callback:
            self.agent.callback(self, connector, reason)


class GraylogAgent:  # pylint: disable=R0903
    """
    Multiple use agent for making calls to a specific Graylog server
    """

    ENV_OPTIONS = {
        "host": "GRAYLOG_HOST",
        "port": "GRAYLOG_PORT",
        "max_age": "GRAYLOG_MAX_AGE",
        "rate_limit": "GRAYLOG_RATE_LIMIT",
    }

    ENV_DEFAULTS = {
        "GRAYLOG_HOST": "localhost",
        "GRAYLOG_PORT": 12201,  # graylog default tcp input
        "GRAYLOG_MAX_AGE": 24 * 60 * 60,  # seconds
        "GRAYLOG_RATE_LIMIT": 100,  # messages per second
    }

    def __init__(self, reactor, **kwargs):
        options = self.options
        options.update(kwargs)

        self.reactor = reactor
        self.host = options["host"]
        self.port = int(options["port"])
        self.rate_limit = int(options["rate_limit"])
        self.max_age = int(options["max_age"])
        self.callback = None
        self.errback = None

    def backoff_secs(self, context):
        """
        Returns the number of seconds to back off sending more messages to
        the Graylog server when the context needs to be rate limited
        """
        if context.message_count < self.rate_limit:
            return 0.0

        return 1.0 / self.rate_limit

    def call_with_backoff(self, callback, context):
        """
        Schedule the callback, after a backoff if the context is rate limited
        """
        self.reactor.callLater(self.backoff_secs(context), callback)

    def call_without_backoff(self, callback):
        """
        Schedule the callback as soon as possible
        """
        self.reactor.callLater(0, callback)

    def send(self, context):
        """
        send the events in a request to the Graylog server in a new connection
        """
        factory = GraylogClientFactory(self, context)
        self.reactor.connectTCP(self.host, self.port, factory)

    def valid_message_age(self, message):
        """
        Test if the age of a message is below the maximum allowed
        """
        try:
            age = time.time() - message["timestamp"]
            return age <= self.max_age

        except KeyError:
            # Messages are not required to have a timestamp
            return True

    @property
    def options(self):
        """
        Get default agent options, overriden by environment variables
        """
        return {
            option: getenv(envvar, default=self.ENV_DEFAULTS[envvar])
            for option, envvar in self.ENV_OPTIONS.items()
        }
