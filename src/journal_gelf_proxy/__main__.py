"""Make the module invokable."""

from .entrypoint import entrypoint

if __name__ == "__main__":
    entrypoint()
