from behave import *
from environment import connect_graylog_agent
from environment import create_graylog_agent
from environment import create_graylog_receiver
from environment import contains_json, match_json
from io import BytesIO
from journal_gelf_proxy.gelf import GELF


@when("converted to a GELF message")
def step_impl(context):
    context.message = GELF(context.events[0])


@when("converted to GELF messages")
def step_impl(context):
    context.messages = [GELF(event) for event in context.events]


@then("the message is")
def step_impl(context):
    actual = str(context.message)
    expected = context.text
    assert match_json(actual, expected), f"expected '{expected}', but got '{actual}'"


@then("the message contains")
def step_impl(context):
    actual = str(context.message)
    expected = context.text
    assert contains_json(actual, expected), f"expected '{expected}', but got '{actual}'"


@then("there are {:d} messages")
def step_impl(context, expected):
    actual = len(context.messages)
    assert actual == expected, f"expected {expected} messages, but got {actual}"


@given("a GELF message")
@when("the expected GELF message is")
def step_impl(context):
    context.message = GELF(json=context.text)


@given("a Graylog agent")
def step_impl(context):
    create_graylog_agent(context)


@given("a Graylog receiver")
def step_impl(context):
    create_graylog_receiver(context)


@when("sent with the agent")
def step_impl(context):
    connect_graylog_agent(context)


@then("it is received by the receiver")
def step_impl(context):
    expected = str(context.message)
    actual, _ = context.receiver.communicate()
    actual = actual[:-1].decode()

    if "proxy_context" in context:
        request = context.proxy_context.request
        response = request.response
        response_code = request.response_code
        assert (
            response_code == 200
        ), f"Would tell client HTTP {response_code}: {response}"

    assert len(actual) > 0, "empty response from receiver"
    assert match_json(expected, actual), f"expected '{expected}', but got '{actual}'"


@then("it is not received by the receiver")
def step_impl(context):
    actual, _ = context.receiver.communicate()
    actual = actual[:-1].decode()

    if "proxy_context" in context:
        request = context.proxy_context.request
        response = request.response
        response_code = request.response_code
        assert (
            context.request.response_code == 200
        ), f"Would tell client HTTP {response_code}: {response}"

    assert len(actual) == 0, f"expected empty response, but got '{actual}'"
