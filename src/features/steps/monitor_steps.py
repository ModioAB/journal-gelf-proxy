from behave import *
from environment import create_monitor, connect_monitor, get_response


@given("a service monitor")
def step_impl(context):
    create_monitor(context)


@when("the service monitor accepts json")
def step_impl(context):
    context.http_accept = "application/json"


@when('the service monitor queries "/-/{endpoint}"')
def step_impl(context, endpoint):
    context.endpoint = endpoint
    connect_monitor(context)


@then("it receives a response without content")
def step_impl(context):
    status_code = get_response(context).status_code

    assert status_code == 204, f"expected HTTP status code 204, got {status_code}"


@then("it receives a GELF message")
def step_impl(context):
    response = get_response(context)
    response.raise_for_status()
    context.message = response.text
