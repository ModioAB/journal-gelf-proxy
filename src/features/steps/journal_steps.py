from behave import *
from codecs import escape_decode
from io import BytesIO
from journal_gelf_proxy.listener import JournalEvents


@given("an input")
def step_impl(context):
    input_string, _ = escape_decode(bytes(context.text, "utf-8"))
    context.input = BytesIO(input_string)


@when(r"parsed as a journal event")
def step_impl(context):
    context.events = list(JournalEvents(context.input))
    length = len(context.events)
    assert length == 1, f"expected 1 event, but got {length}"


@when(r"parsed as journal events")
def step_impl(context):
    context.events = list(JournalEvents(context.input))
    length = len(context.events)
    assert length >= 1, f"expected at least 1 event, but got {length}"


@then("it contains")
def step_impl(context):
    for event in context.events:
        expected = len(context.table.rows)
        actual = len(event)
        assert actual == expected, f"expected {expected} rows, but got {actual}"

        for row in context.table:
            key = row["key"]
            expected, _ = escape_decode(bytes(row["value"], "utf-8"))
            actual = event[key]

            assert (
                actual == expected
            ), f"expected '{key}' to be '{expected}', but got '{actual}'"


@then("there are {:d} events")
def step_impl(context, expected):
    actual = len(context.events)

    assert actual == expected, f"expected {expected} events, but got {actual}"
