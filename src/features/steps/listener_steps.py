from behave import *
from environment import create_client, create_listener, connect_client, get_response


@given("a journal upload client")
def step_impl(context):
    create_client(context)


@given("a journal upload listener")
def step_impl(context):
    create_listener(context)


@then("the client can connect to the listener")
def step_impl(context):
    connect_client(context)


@when("uploaded by the client")
def step_impl(context):
    connect_client(context)


@then("the listener accepts the events")
def step_impl(context):
    get_response(context).raise_for_status()
