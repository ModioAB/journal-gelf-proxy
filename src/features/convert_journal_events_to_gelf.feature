Feature: Convert Journal Events to GELF

    Scenario: A Single Journal Event in Text Format

        Given an input:
            """
            MESSAGE=text message
            \n
            """
        When parsed as a journal event
         And converted to a GELF message
        Then the message is:
            """
            {"version":"1.1", "host":"localhost", "short_message":"text message"}
            """

    Scenario: A Single Journal Event in Binary Format

        Given an input:
            """
            MESSAGE
            \x0e\x00\x00\x00\x00\x00\x00\x00binary message
            \n
            """
        When parsed as a journal event
         And converted to a GELF message
        Then the message is:
            """
            {"version":"1.1", "host":"localhost", "short_message":"binary message"}
            """

    Scenario: A Stream of Journal Events

        Given an input:
            """
            MESSAGE=text message

            MESSAGE
            \x0e\x00\x00\x00\x00\x00\x00\x00binary message
            \n
            """
        When parsed as journal events
         And converted to GELF messages
        Then there are 2 messages

    Scenario: Journal hostname is mapped to GELF host

        Given an input:
            """
            _HOSTNAME=client.example.com
            \n
            """
        When parsed as a journal event
         And converted to a GELF message
        Then the message is:
            """
            {"version":"1.1", "host":"client.example.com", "short_message":""}
            """

    Scenario: Journal syslog facility number is mapped to GELF facility

        Given an input:
            """
            SYSLOG_FACILITY=16
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"", "_facility":"local0"}
             """

    Scenario: Journal realtime timestamp is converted to GELF timestamp

        Given an input:
            """
            __REALTIME_TIMESTAMP=1543836539986312
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"", "timestamp":1543836539.986312}
             """

    Scenario: Journal priority is mapped to GELF level

        Given an input:
            """
            PRIORITY=3
            \n
            """
        When parsed as a journal event
         And converted to a GELF message
        Then the message is:
            """
            {"version":"1.1", "host":"localhost", "short_message":"", "level":3}
            """

    Scenario: Journal cursor is not included in the GELF message

        Given an input:
            """
            __CURSOR=991D64EB-001A-4B58-B585-D6EC3C6AD72C
            MESSAGE=no cursors included
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"no cursors included"}
             """

    Scenario: Journal event contains binary data

        Given an input:
            """
            MESSAGE
            \x04\x00\x00\x00\x00\x00\x00\x00\xAB\xAD\xCA\xFE
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"abadcafe"}
             """

    Scenario: Journal event contains a known numeric syslog facility

        Given an input:
            """
            SYSLOG_FACILITY=0
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"", "_facility":"kern"}
             """

    Scenario: Journal event contains an unknown numeric syslog facility

        Given an input:
            """
            SYSLOG_FACILITY=99
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"", "_facility":"unknown(99)"}
             """

    Scenario: Journal event contains a syslog factility as text

        Given an input:
            """
            SYSLOG_FACILITY=coffee
            \n
            """
         When parsed as a journal event
          And converted to a GELF message
         Then the message is:
             """
             {"version":"1.1", "host":"localhost", "short_message":"", "_facility":"coffee"}
             """
