from behave import *
import json
import requests
import subprocess
from twisted.internet import reactor
from journal_gelf_proxy.gelf import GraylogAgent
from journal_gelf_proxy.listener import Context as RequestContext


def connect_client(context):
    request = create_request(context)

    try:
        stdout, _ = context.listener.communicate(timeout=1.0)
        if stdout:
            print(stdout)

    except subprocess.TimeoutExpired:
        if "input" in context:
            request.response = context.client.post(
                "http://localhost:19532/upload",
                data=context.input,
                headers={"Content-Type": "application/vnd.fdo.journal"},
                timeout=1.0,
            )
        else:
            request.response = context.client.post("http://localhost:19532")


def create_client(context):
    context.client = requests


def create_listener(context):
    if "receiver" in context:
        command = ["python3", "-m", "test.journal_to_gelf"]
    else:
        command = ["python3", "-m", "test.journal_listener"]

    context.listener = subprocess.Popen(command, stdout=subprocess.PIPE)


def connect_monitor(context):
    request = create_request(context)
    headers = {}
    if "http_accept" in context:
        headers["Accept"] = context.http_accept

    try:
        stdout, _ = context.listener.communicate(timeout=1.0)
        if stdout:
            print(stdout)

    except subprocess.TimeoutExpired:
        endpoint = context.endpoint
        request.response = context.client.get(
            f"http://localhost:19532/-/{endpoint}",
            headers=headers,
            timeout=1.0,
        )


def create_monitor(context):
    context.client = requests


def connect_graylog_agent(context):
    def connection_errback(_a, _b, reason):
        if reactor.running:
            reactor.stop()
        raise reason

    create_request(context)
    context.graylog_agent.errback = connection_errback

    try:
        stdout, _ = context.receiver.communicate(timeout=1.0)
        if stdout:
            print(stdout)
        raise
    except subprocess.TimeoutExpired:
        context.graylog_agent.send(context.request_context)
        reactor.run()


def create_graylog_agent(context):
    context.graylog_agent = GraylogAgent(reactor)


def create_graylog_receiver(context):
    command = ["nc", "-l", "127.0.0.1", "12201"]
    context.receiver = subprocess.Popen(command, stdout=subprocess.PIPE)


class NullLogger:
    def __init__(self, *_):
        pass

    def error(self, **_):
        pass

    def info(self, **_):
        pass


class TestRequest:
    def __init__(self):
        self.response = None
        self.response_code = None

    def finish(self):
        if reactor.running:
            reactor.stop()

    def setResponseCode(self, code):
        self.response_code = code

    def write(self, response):
        self.response = response


def create_request(context):
    if "messages" in context:
        messages = context.messages
    elif "message" in context:
        messages = [context.message]
    else:
        messages = []

    request_context = RequestContext()
    request_context.messages = iter(messages)
    request_context.request = TestRequest()
    request_context.log = NullLogger()
    context.request_context = request_context

    return request_context.request


def after_scenario(context, scenario):
    if "listener" in context:
        context.listener.terminate()
    if "receiver" in context:
        context.receiver.terminate()


def contains_json(this, that):
    try:
        this_dict = json.loads(this)
        that_dict = json.loads(that)

        for key, value in that_dict.items():
            if this_dict[key] != value:
                raise ValueError

    except (KeyError, ValueError):
        return False
    except json.decoder.JSONDecodeError:
        return False
    else:
        return True


def match_json(this, that):
    try:
        return json.loads(this) == json.loads(that)

    except json.decoder.JSONDecodeError:
        return False


def get_request(context):
    if "request_context" in context:
        return context.request_context.request

    return None


def get_response(context):
    request = get_request(context)
    if request:
        return request.response

    return None
