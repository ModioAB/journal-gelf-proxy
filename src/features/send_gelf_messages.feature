@CI_WIP
Feature: Sending GELF messages to a Graylog server


@wip # https://gitlab.com/ModioAB/journal-gelf-proxy/-/issues/4

    Scenario: A GELF message is sent to a TCP endpoint

        Given a GELF message:
            """
            {"version":"1.1", "host":"localhost", "short_message":"test message"}
            """
          And a Graylog agent
          And a Graylog receiver
         When sent with the agent
         Then it is received by the receiver

    Scenario: Journal events are forwarded as GELF messages

        Given a Graylog receiver
          And a journal upload client
          And a journal upload listener
          And an input
            """
            MESSAGE=test message
            \n
            """
         When uploaded by the client
          And the expected GELF message is
            """
            {"version":"1.1", "host":"localhost", "short_message":"test message"}
            """
         Then it is received by the receiver

    Scenario: Outdated journal events are not forwarded

        Given a Graylog receiver
          And a journal upload client
          And a journal upload listener
          And an input
            """
            __REALTIME_TIMESTAMP=946681200000000
            MESSAGE=dropped message
            \n
            """
         When uploaded by the client
         Then it is not received by the receiver
