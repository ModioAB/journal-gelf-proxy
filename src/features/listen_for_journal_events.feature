Feature: Listen for Journal Events to be Uploaded

    Background: We have a client and a listener

        Given a journal upload client
        And a journal upload listener

    Scenario: A journal upload client connects to the listener

        Then the client can connect to the listener

    Scenario: A journal upload client uploads events to the listener

        Given an input
            """
            MESSAGE=text message

            MESSAGE
            \x0e\x00\x00\x00\x00\x00\x00\x00binary message
            \n
            """
        When uploaded by the client
        Then the listener accepts the events
