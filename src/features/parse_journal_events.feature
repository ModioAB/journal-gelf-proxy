Feature: Parse Journal Events

    Scenario: A Single Journal Event in Text Format

        Given an input
            """
            MESSAGE=text message
            \n
            """
        When parsed as a journal event
        Then it contains
            | key       | value         |
            | MESSAGE   | text message  |

    Scenario: A Single Journal Event in Binary Format

        Given an input
            """
            MESSAGE
            \x0e\x00\x00\x00\x00\x00\x00\x00binary message
            \n
            """
        When parsed as a journal event
        Then it contains
            | key       | value             |
            | MESSAGE   | binary message    |

    Scenario: A Stream of Journal Events

        Given an input
            """
            MESSAGE=text message

            MESSAGE
            \x0e\x00\x00\x00\x00\x00\x00\x00binary message
            \n
            """
        When parsed as journal events
        Then there are 2 events

    Scenario: Journal event contains binary data

        Given an input:
            """
            MESSAGE
            \x04\x00\x00\x00\x00\x00\x00\x00\xAB\xAD\xCA\xFE
            \n
            """
         When parsed as a journal event
         Then it contains
             | key      | value             |
             | MESSAGE  | \xAB\xAD\xCA\xFE  |
