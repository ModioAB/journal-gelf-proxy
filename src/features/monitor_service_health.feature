Feature: Listen for Health Checks

    The journal upload service has monitoring endpoints, that an outside
    monitoring agent can query to determine service health and readiness.

    Background: We have a monitor and a listener

        Given a service monitor
          And a journal upload listener

    Scenario: A service monitor performs a liveness check

        When the service monitor queries "/-/livez"
        Then it receives a response without content

    Scenario: A service monitor performs a liveness status query

        When the service monitor accepts json
         And the service monitor queries "/-/livez"
        Then it receives a GELF message
         And the message contains
            """
            {
                "version":          "1.1",
                "host":             "localhost",
                "short_message":    "listening for connections",
                "_alive":           true,
                "_ready":           null
            }
            """
