from twisted.internet import task
from journal_gelf_proxy.listener import journal_upload_listener
from journal_gelf_proxy.gelf import GraylogAgent


def gelf_proxy(reactor):
    """
    Create a Twisted Internet Endpoint that listens for journal uploads and proxies to GELF
    """
    graylog_agent = GraylogAgent(reactor)

    return journal_upload_listener(
        reactor, "tcp:19532:interface=127.0.0.1", graylog_agent
    )


if __name__ == "__main__":
    task.react(gelf_proxy)
