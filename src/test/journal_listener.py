from twisted.internet import task
from journal_gelf_proxy.listener import journal_upload_listener

if __name__ == "__main__":
    task.react(journal_upload_listener, ("tcp:19532:interface=127.0.0.1",))
