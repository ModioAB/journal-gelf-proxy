FROM scratch
MAINTAINER "Calle Englund" <calle@modio.se>
EXPOSE 19532

env LANG C.UTF-8
env LC_CTYPE C.utf8

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE

ADD rootfs.tar /
RUN python3 -m pip install --no-index --find-links=/build/ journal_gelf_proxy

CMD ["/usr/local/bin/journal_gelf_proxy"]
