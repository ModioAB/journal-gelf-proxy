IMAGE_REPO = registry.gitlab.com/modioab/journal-gelf-proxy
FEDORA_ROOT_ARCHIVE = rootfs.tar
FEDORA_ROOT_RELEASE ?= dummy

IMAGE_FILES = $(FEDORA_ROOT_ARCHIVE)
IMAGE_BUILD_VOLUME = $(realpath wheel)

FEDORA_ROOT_PACKAGES = python3
FEDORA_ROOT_PACKAGES += python3-twisted
FEDORA_ROOT_PACKAGES += python3-pip

test: IMAGE_TEST_CMD = /usr/local/bin/journal_gelf_proxy --test-run
test: load test-image


CLEANUP_FILES += wheel
# This allows CI to run without building wheels, even if the wheel-files dont match.
ifeq ($(HAVE_ARTIFACTS),)
IMAGE_FILES += wheel
endif

# This figures out the package name & version by calling setuptools. It allows
# us to know the exact name of the wheel file needed.
PKG_VERSION ?= $(shell python3 -c "from setuptools import setup;setup()" --version 2>/dev/null)
PKG_WHEEL   := journal_gelf_proxy-${PKG_VERSION}-py3-none-any.whl

wheel/${PKG_WHEEL}: setup.cfg pyproject.toml src/journal_gelf_proxy/*.py
	pip wheel --wheel-dir=wheel .

.PHONY: wheel
wheel: wheel/${PKG_WHEEL}


include build.mk
